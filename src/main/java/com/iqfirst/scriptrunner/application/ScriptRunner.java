package com.iqfirst.scriptrunner.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ScriptRunner {
	public static void main (String args[]){
		SpringApplication.run(ScriptRunner.class, args);
	}
}
