package com.iqfirst.scriptrunner.application;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScriptExecutor {
	private static final Logger log = LoggerFactory.getLogger(ScriptExecutor.class);
	@Scheduled(cron = "20 21 00 * * ?")
	public void scriptExecutor(){
		try{
			Path path = Paths.get("scripts").toAbsolutePath().normalize();
			Path scriptPath = path.resolve("copy.sh").normalize();
			log.info("file found at :"+scriptPath.toAbsolutePath());
			String absPath = scriptPath.toAbsolutePath().toString().replace("'\'", "\\");
			log.info("Trying to execute the script.......");
			Runtime rt = Runtime.getRuntime();
			Process p = rt.exec("cmd /c start "+absPath, null, new File("E:\\test\\"));
			StringBuilder output = new StringBuilder();

	        BufferedReader reader = new BufferedReader(
	                new InputStreamReader(p.getInputStream()));

	        String line;
	        while ((line = reader.readLine()) != null) {
	            output.append(line + "\n");
	        }

	        int exitVal = p.waitFor();
	        if (exitVal == 0) {
	        	log.info("Successfully copied file from server!");
	        	log.info(output.toString());
	        } else {
	            log.info("Unable to execute the copy script from script file");
	        }
	        log.info(output.toString());
	        log.info("Script block finish......");

		}catch(Exception ex){
			log.error("There is an exception occured while trying to copy file from server to local. Error: "+ex.getMessage());
		}
	}

}
