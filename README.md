Need to generate ssh key pair using putty-gen nee not to use any pass-phrase for this.

Then copy the public key to a ~/.ssh/authorized_keys file on the server

save the private key to local .ssh folder

Then convert the id_rsa file of local machine to appropriate format by using below command

ssh-keygen -f id_rsa -y > id_rsa.pub.

No we can use ssh command without password to remote server